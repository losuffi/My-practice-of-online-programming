#include <iostream>
#include <string>
using namespace std;
/**
 * 定义类：Student
 * 数据成员：m_strName
 * 无参构造函数：Student()
 * 有参构造函数：Student(string _name)
 * 拷贝构造函数：Student(const Student& stu)
 * 析构函数：~Student()
 * 数据成员函数：setName(string _name)、getName()
 */
 class Student()
 {
     public:
        Student();
        Student(string _name);
        Student(const Student &stu);
        ~Student();
        void setName(string _name);
        string getName();
    private:
        string m_Name;
 };
void Student::setName(string _name){m_Name=_name;}
string Student::getName(){return m_name;}
Student::Student(){cout<<"none"<<endl;}
Student::Student(string _name){cout<<"parameter is"<<_name<<endl;}
Student::Student(const Student &stu){cout<<"copy function"<<endl;}
Student::~Student(){cout<<"project destroied!"<<endl;}

int main(void)
{
    // 通过new方式实例化对象*stu
    Student *stu = new Student;
    // 更改对象的数据成员为“慕课网”
	stu->setName("慕课网")
    cout<<stu->getName<<endl;
	delete stu;
	return 0;
}